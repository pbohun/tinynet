;;;; tinynet.asd

(asdf:defsystem #:tinynet
  :description "Describe tinynet here"
  :author "Philip Bohun"
  :license  "BSD"
  :version "0.0.1"
  :serial t
  :depends-on (#:cffi #:str #:usocket)
  :components ((:file "package")
               (:file "tinynet")))
