# tinynet
### _Philip Bohun_

This project is designed to create a very simple HTTP 1.1 library for SBCL Common Lisp. 
Because it's HTTP 1.1 it does not have the capability for SSL/TLS, so don't expose servers you create with it to the internet!
It's not designed for portability, speed, or serious production purposes. 
Rather, it's designed to be small and easy to understand, so that the user can create simple, local, HTTP servers and clients on their local machine. 

## Requirements
* SBCL
* quicklisp https://www.quicklisp.org/beta/

## Installation
Navigate to your `local-projects` directory and pull a copy of the project:
### _Example_
```
cd ~/quicklisp/local-projects
git clone https://gitlab.com/pbohun/tinynet.git
```

Once you've done that you can now load through quicklisp (e.g. `(ql:quickload :tinynet)`)

## Versions
You may have seen videos describing this project. 
You can see the progress made from each video by checking `git log`.
There will be various commits corresponding to the videos you saw. 

## License

BSD

