;;;; tinynet.lisp

(in-package #:tinynet)

(require "uiop")

;; TODO (phil): change to defconstant when done
(defparameter +http-version+ "HTTP/1.1")
(defparameter +sigint+ 2)
(defparameter *server-running* nil)

;; UTILITY MACROS
;; ----------------------------------------------------------------------------------
(defmacro set-signal-handler (signo &body body)
  (let ((handler (gensym "HANDLER")))
    `(progn
       (cffi:defcallback ,handler :void ((signo :int))
         (declare (ignore signo))
         ,@body)
       (cffi:foreign-funcall "signal" :int ,signo :pointer (cffi:callback ,handler)))))

;; UTILITY FUNCTIONS
;; ----------------------------------------------------------------------------------
(set-signal-handler +sigint+
  (format t "shutting down server...~%")
  (setf *server-running* nil))

(defun string->keyword (s)
  "Converts a string to a keyword."
  (intern (string-upcase s) :keyword))

(defun headers->string (headers)
  "Converts an alist to an http formated header list."
  (format nil "~{~A~}" (mapcar (lambda (h) (header->string h)) headers)))

(defun header->string (header)
  "Convert a single alist entry to an http formatted header line."
  (format nil "~A:~A~A~A" (car header) (cdr header) #\return #\newline))

(defun build-http-req (method request-target headers content)
  "Returns an http request string."
  (format nil "~A ~A ~A~A~A~A~A~A~A"
	  method
	  request-target
	  +http-version+
	  #\return #\newline
	  (or (headers->string headers) "")
	  #\return #\newline
	  (or content "")))

(defun build-http-resp (status-code reason-phrase headers content)
  "Returns an http response string." 
  (format nil "~A ~A ~A~A~A~A~A~A~A"
	  +http-version+
	  status-code
	  reason-phrase
	  #\return #\newline
	  (or (headers->string headers) "")
	  #\return #\newline
	  (or content "")))

(defun read-start-line (stream)
  "Parses an http start line and returns a list of its components."
  (let ((line (str:trim (read-line stream))))
    (uiop:split-string line :separator '(#\space))))

(defun parse-header (header-line)
  "Parses a single header into a plist."
  (let ((parts (uiop:split-string (str:trim header-line) :separator '(#\:))))
    (list (string->keyword (str:trim (car parts))) (str:trim (cadr parts)))))

(defun flatten-list (lst result)
  "Flattens an alist into a plist. Used to flatten a headers list."
  (cond
    ((null lst) result)
    (t (flatten-list (cdr lst)
		     (let* ((header (car lst))
			    (header-key (nth 0 header))
			    (header-val (nth 1 header)))
		       (push header-val result)
		       (push header-key result)
		       result)))))

(defun read-headers (stream result)
  "Reads header lines from a string and returns them as a plist."
  (let ((line (read-line stream)))
    (cond
      ((null line) result)
      ((= (length (str:trim line)) 0) result)
      (t (read-headers stream (push (parse-header line) result))))))

(defun parse-http-req (req)
  "Reads and parses an http request from a stream. Returns a plist of the parsed request."
  (let ((req-line (read-start-line req))
	(headers (read-headers req nil)))
    (list :method (nth 0 req-line)
	  :request-target (nth 1 req-line)
	  :http-version (nth 2 req-line)
	  :headers (flatten-list headers nil)
	  :content req)))

(defun parse-http-resp (resp)
  "Reads and parses an http response from a stream. Returns a plist of the parsed response."
  (let ((resp-line (read-start-line resp))
	(headers (read-headers resp nil)))
    (list :http-version (nth 0 resp-line)
	  :status-code (nth 1 resp-line)
	  :reason-phrase (nth 2 resp-line)
	  :headers (flatten-list headers nil)
	  :content resp)))

(defun get-path (s)
  (remove-if (lambda (s) (= (length s) 0))
	     (uiop:split-string s :separator "/")))

(defun add-handle-func (server route-string func)
  (push (list route-string func) (getf server :route-table))
  (setf (getf server :route-table)
	(sort (getf server :route-table)
	      (lambda (e1 e2)
		(let ((p1 (get-path e1))
		      (p2 (get-path e2)))
		  (> (length p1) (length p2))))
	      :key #'car)))

(defun match-paths (p1 p2)
  (cond ((null p1) t)
	((null p2) nil)
	((string= (car p1) (car p2)) (match-paths (cdr p1) (cdr p2)))
	(t nil)))

(defun select-handle-func (url server)
  (let ((path (get-path url)))
    (labels ((select-handle (route-table)
	       (cond ((null route-table) nil)
		     (t (let* ((handler (car route-table))
			       (p (get-path (car handler))))
			  (if (match-paths p path)
			      handler
			      (select-handle (cdr route-table))))))))
      (select-handle (getf server :route-table)))))

(defun create-server (port)
  (let* ((socket (usocket:socket-listen "127.0.0.1" port))
	 (connection (usocket:socket-accept socket :element-type 'character)))
    (unwind-protect
         (progn
	   (format t "~A~%" (parse-http-req (usocket:socket-stream connection)))
	   (format (usocket:socket-stream connection)
		   (build-http-resp 200 "OK" '((:content-type . "text/plain") (:content-length . (length "hello there"))) "hello there"))
	   (force-output (usocket:socket-stream connection)))
      (progn
	(format t "Closing sockets~%")
	(usocket:socket-close connection)
        (usocket:socket-close socket)))))


(defun test-loop ()
  (setf *server-running* t)
  (do ()
      ((null *server-running*))
    (format t "handling connection...~%")
    (sleep 1))
  (format t "server stopped~%"))


